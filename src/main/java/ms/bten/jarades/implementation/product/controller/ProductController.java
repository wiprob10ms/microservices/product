package ms.bten.jarades.implementation.product.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import ms.bten.jarades.implementation.product.dto.ProductDTO;
import ms.bten.jarades.implementation.product.exception.NoRecordFoundException;
import ms.bten.jarades.implementation.product.service.ProductService;

@RequestMapping("products")
@RestController
@RequiredArgsConstructor
public class ProductController {
	
	private final ProductService productService;
	
	private static final String PRODUCT_NOT_FOUND_BY_CODE = "No Product Records Found For Given Product Code";
	private static final String PRODUCT_NOT_FOUND = "No Product Records Found";

	@PostMapping
	public ResponseEntity<ProductDTO> saveProduct(@Valid @RequestBody ProductDTO product) {
		return ResponseEntity.ok(productService.saveProduct(product));
	}

	@GetMapping
	public ResponseEntity<List<ProductDTO>> getAllProducts() {
		List<ProductDTO> products = productService.getAllProducts();

		if (products.isEmpty())
			throw new NoRecordFoundException(PRODUCT_NOT_FOUND);

		return ResponseEntity.ok(products);
	}

	@GetMapping("{productCode}")
	public ResponseEntity<ProductDTO> getProductByCode(@PathVariable("productCode") String code) {
		Optional<ProductDTO> product = productService.getProductByCode(code);

		if (product.isPresent())
			return ResponseEntity.ok(product.get());

		throw new NoRecordFoundException(PRODUCT_NOT_FOUND_BY_CODE);
	}

	@PutMapping("{productCode}")
	public ResponseEntity<ProductDTO> updateProductByCode(@PathVariable("productCode") String code,
			@Valid @RequestBody ProductDTO product) {
		product.setProductCode(code);

		Optional<ProductDTO> productDTO = productService.updateProductByCode(product);

		if (productDTO.isPresent())
			return ResponseEntity.ok(productDTO.get());

		throw new NoRecordFoundException(PRODUCT_NOT_FOUND_BY_CODE);
	}

	@DeleteMapping("{productCode}")
	public HttpStatus deleteProductByCode(@PathVariable("productCode") String code) {
		if (productService.productIsDeletedByCode(code))
			return HttpStatus.OK;

		throw new NoRecordFoundException(PRODUCT_NOT_FOUND_BY_CODE);
	}

}
