package ms.bten.jarades.implementation.product.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ProductDTO {
	
	@Size(min = 8, max = 8)
	String productCode;
	@NotNull
	String name;
	@NotNull
	String brand;
	@NotNull
	String description;
	@NotNull
	String category;
	@NotNull
	int measureValue;
	@NotNull
	String measureUnit;
	@NotNull
	BigDecimal basePrice;
	@NotNull
	BigDecimal initQuantity;
}
