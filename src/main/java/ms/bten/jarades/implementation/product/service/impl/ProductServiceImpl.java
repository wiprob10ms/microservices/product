package ms.bten.jarades.implementation.product.service.impl;

import static ms.bten.jarades.implementation.product.util.ServiceUtils.convertFromObjectToClass;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.RequiredArgsConstructor;
import ms.bten.jarades.implementation.product.dto.InventoryRequestDTO;
import ms.bten.jarades.implementation.product.dto.PriceRequestDTO;
import ms.bten.jarades.implementation.product.dto.ProductDTO;
import ms.bten.jarades.implementation.product.dto.feign.InventoryClient;
import ms.bten.jarades.implementation.product.dto.feign.ProductPriceClient;
import ms.bten.jarades.implementation.product.entity.Product;
import ms.bten.jarades.implementation.product.repository.ProductRepository;
import ms.bten.jarades.implementation.product.service.ProductService;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;

	@Value("${endpoints.services.inventory}")
	private String inventoryEndPoint;
	@Value("${endpoints.services.price}")
	private String priceEndPoint;
	@Value("${endpoints.zuul.uri}")
	private String zuulUri;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public ProductDTO saveProduct(ProductDTO productDTO) {
		Product saved = productRepository.save(convertFromObjectToClass(productDTO, Product.class));

		ProductPriceClient priceClient = Feign.builder()
				.encoder(new JacksonEncoder()).decoder(new JacksonDecoder())
				.target(ProductPriceClient.class, this.zuulUri + priceEndPoint);

		PriceRequestDTO priceReqDTO = new PriceRequestDTO(
				productDTO.getProductCode(), LocalDate.now(), productDTO.getBasePrice());

		priceClient.postPrice(priceReqDTO);
		
		
		InventoryClient inventoryClient = Feign.builder()
				.encoder(new JacksonEncoder()).decoder(new JacksonDecoder())
				.target(InventoryClient.class, this.zuulUri + inventoryEndPoint);

		InventoryRequestDTO inventoryReqDTO = new InventoryRequestDTO(
				productDTO.getProductCode(), productDTO.getInitQuantity());

		inventoryClient.postInventory(inventoryReqDTO);

		return convertFromObjectToClass(saved, ProductDTO.class);
	}

	@Override
	public List<ProductDTO> getAllProducts() {
		List<Product> products = productRepository.findAll();

		return products.stream().map(p -> (ProductDTO) convertFromObjectToClass(p, ProductDTO.class))
				.collect(Collectors.toList());
	}

	@Override
	public Optional<ProductDTO> getProductByCode(String code) {
		Optional<Product> product = productRepository.findByProductCode(code);

		if (product.isPresent())
			return Optional.of(convertFromObjectToClass(product.get(), ProductDTO.class));

		return Optional.empty();
	}

	@Override
	public Optional<ProductDTO> updateProductByCode(ProductDTO productDTO) {
		Optional<Product> productOpt = productRepository.findByProductCode(productDTO.getProductCode());

		if (productOpt.isPresent()) {
			Product product = convertFromObjectToClass(productDTO, Product.class);
			product.setID(productOpt.get().getID());
			Product saved = productRepository.save(product);
			return Optional.of(convertFromObjectToClass(saved, ProductDTO.class));
		}

		return Optional.empty();
	}

	@Override
	public boolean productIsDeletedByCode(String code) {
		Optional<Product> productOpt = productRepository.findByProductCode(code);

		if (productOpt.isPresent()) {
			productRepository.delete(productOpt.get());
			return true;
		}

		return false;
	}

}
