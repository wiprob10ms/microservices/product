package ms.bten.jarades.implementation.product.dto.feign;

import feign.Headers;
import feign.RequestLine;
import ms.bten.jarades.implementation.product.dto.InventoryRequestDTO;
import ms.bten.jarades.implementation.product.dto.InventoryResponseDTO;

public interface InventoryClient {
	@RequestLine("POST")
	@Headers("Content-Type: application/json")
	InventoryResponseDTO postInventory(InventoryRequestDTO inventoryRequestDTO);
}
