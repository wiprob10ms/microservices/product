package ms.bten.jarades.implementation.product.dto.feign;

import feign.Headers;
import feign.RequestLine;
import ms.bten.jarades.implementation.product.dto.PriceRequestDTO;
import ms.bten.jarades.implementation.product.dto.PriceResponseDTO;

public interface ProductPriceClient {
	@RequestLine("POST")
	@Headers("Content-Type: application/json")
	PriceResponseDTO postPrice(PriceRequestDTO productPriceDTO);
}
