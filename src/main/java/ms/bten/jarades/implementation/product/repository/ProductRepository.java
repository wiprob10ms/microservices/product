package ms.bten.jarades.implementation.product.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ms.bten.jarades.implementation.product.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	Optional<Product> findByProductCode(String productCode);

}
