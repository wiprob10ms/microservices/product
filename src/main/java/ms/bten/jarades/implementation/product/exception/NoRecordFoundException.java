package ms.bten.jarades.implementation.product.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoRecordFoundException extends RuntimeException {

	public NoRecordFoundException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
