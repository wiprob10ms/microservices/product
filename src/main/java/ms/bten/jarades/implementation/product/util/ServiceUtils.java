package ms.bten.jarades.implementation.product.util;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class ServiceUtils {
	
	private static final ModelMapper modelMapper = new ModelMapper();
	
	@SuppressWarnings("unchecked")
	public static <T> T convertFromObjectToClass(Object object, Class<?> clazz) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
	    return (T) modelMapper.map(object, clazz);
	}

}
