package ms.bten.jarades.implementation.product.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import ms.bten.jarades.implementation.product.entity.Product;
import ms.bten.jarades.implementation.product.repository.ProductRepository;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ProductConfiguration implements CommandLineRunner {
	
	@Autowired
	ProductRepository productRepository;
	
	@Override
	public void run(String... args) throws Exception {		
		Product p1 = new Product(null, "SCNB6728", "Beer", "Heineken", "pure malt beer", "FOOD", 330, "ml");
		Product p2 = new Product(null, "HIJK5617", "iPhone", "Apple", "iPhone 11S Pro Max 512GB", "TECH", 1, "un");
		Product p3 = new Product(null, "PXTR8375", "toilet paper", "Charmin", "Ultra Strong Toilet Paper 12 Super Mega Roll, 426 Sheets Per Roll", "Household Essentials", 12, "un");
		Product p4 = new Product(null, "SLBQ6708", "Glass Cleaner", "Windex", "Refill, Original Blue", "Household Essentials", 2000, "ml");
		
		productRepository.saveAll(Arrays.asList(p1,p2,p3,p4));
	}
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build();                                           
    }
	
	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		return new RestTemplateBuilder().build();
	}

}
