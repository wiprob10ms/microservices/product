package ms.bten.jarades.implementation.product.util.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class ProductAspect {
	
	@Before("execution(* ms.bten.jarades.implementation.product.*.*.*(..))")
	public void before(JoinPoint jp) {
		log.info(" --------->>>| Before method: {} in class: {}. Args: {}", 
				jp.getSignature().getName(), 
				jp.getSignature().getDeclaringTypeName(),
				Arrays.deepToString(jp.getArgs()));	
	}
	
	@AfterReturning(pointcut = "execution(* ms.bten.jarades.implementation.product.*.*.*(..))",
			returning="result")
	public void after(JoinPoint jp, Object result) {
		log.info(" ---------|<<< After method: {} in class: {}. result: {}", 
				jp.getSignature().getName(), 
				jp.getSignature().getDeclaringTypeName(), result);	
	}

}
