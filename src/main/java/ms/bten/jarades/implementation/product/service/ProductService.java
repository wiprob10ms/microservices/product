package ms.bten.jarades.implementation.product.service;

import java.util.List;
import java.util.Optional;

import ms.bten.jarades.implementation.product.dto.ProductDTO;

public interface ProductService {

	ProductDTO saveProduct(ProductDTO product);
	
	List<ProductDTO> getAllProducts();

	Optional<ProductDTO> getProductByCode(String code);

	Optional<ProductDTO> updateProductByCode(ProductDTO product);

	boolean productIsDeletedByCode(String code);

}