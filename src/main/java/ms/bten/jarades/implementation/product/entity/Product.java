package ms.bten.jarades.implementation.product.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "PRODUCT")
@AllArgsConstructor
@NoArgsConstructor
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long ID;
	@Column(name = "PRODUCT_CODE", unique=true)
	String productCode;
	@Column
	String name;
	@Column
	String brand;
	@Column
	String description;
	@Column
	String category;
	@Column(name = "MEASURE_VALUE")
	int measureValue;
	@Column(name = "MEASURE_UNIT")
	String measureUnit;
	
}
