package ms.bten.jarades.implementation.product.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PriceRequestDTO {
	private String productId;
	private LocalDate dateFrom;
	private BigDecimal price;
}
