package ms.bten.jarades.implementation.product.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import ms.bten.jarades.implementation.product.entity.Product;

@DataJpaTest
public class ProductRepositoryTest {
	
	@Autowired
	TestEntityManager entityManager;
	
	@Autowired
	ProductRepository productRepository;
	
	@Test
	public void testFindByProductCode () {
		// given
		Product product = new Product(null, "SCNB6728", "Beer", "Heineken", "pure malt beer", "FOOD", 330, "ml");
        entityManager.persist(product);
        entityManager.flush();
        
        // when
        Optional<Product> returnProduct = productRepository.findByProductCode(product.getProductCode());
        
        // Then
        assertThat(returnProduct.isPresent()).isTrue();
        assertThat(returnProduct.get().getProductCode()).isEqualTo(product.getProductCode());
	}
	
}
