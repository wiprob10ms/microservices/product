package ms.bten.jarades.implementation.product.controller;

import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ms.bten.jarades.implementation.product.controller.ProductController;
import ms.bten.jarades.implementation.product.dto.ProductDTO;
import ms.bten.jarades.implementation.product.entity.Product;
import ms.bten.jarades.implementation.product.util.ServiceUtils;

@WebMvcTest(ProductController.class)
public class ProductControllerTest {
	
	private static final String BASE_URI_PATH = "http://localhost:8080/products/";
	private static final ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private MockMvc mvc;

	@MockBean
	private ProductController productController;
	
	Product product;
	ProductDTO productDTO;
	
	@BeforeEach
	public void init() {
		product = new Product(null, "SCNB6728", "Beer", "Heineken", "pure malt beer", "FOOD", 330, "ml");
		productDTO = ServiceUtils.convertFromObjectToClass(product, ProductDTO.class);
		productDTO.setInitQuantity(BigDecimal.TEN);
		productDTO.setBasePrice(BigDecimal.TEN);
	}
	
	@Test
	public void getAllProducts_success() throws Exception {
		
		List<ProductDTO> listProductDTO = singletonList(productDTO);

		ResponseEntity<List<ProductDTO>> allProducts = 
				new ResponseEntity<>(listProductDTO, HttpStatus.OK);

		given(productController.getAllProducts()).willReturn(allProducts);

		mvc.perform(
				get(BASE_URI_PATH)
				.contentType(APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].productCode", is(productDTO.getProductCode())));
	}
	
	@Test
	public void getProductByCode_success() throws Exception {

		ResponseEntity<ProductDTO> productResponse = 
				new ResponseEntity<>(productDTO, HttpStatus.OK);

		given(productController.getProductByCode(productDTO.getProductCode()))
		.willReturn(productResponse);

		mvc.perform(
				get(BASE_URI_PATH + productDTO.getProductCode())
				.contentType(APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("productCode", is(productDTO.getProductCode())));
	}
	
	@Test
	public void postProduct_success() throws Exception {

		ResponseEntity<ProductDTO> productsResponse = 
				new ResponseEntity<>(productDTO, HttpStatus.OK);
		
		given(productController.saveProduct(productDTO))
				.willReturn(productsResponse);

		mvc.perform(
				post(BASE_URI_PATH)
				.content(mapper.writeValueAsString(productDTO))
				.contentType(APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void postProduct_invalidCode() throws Exception {
		
		productDTO.setProductCode("123ABC");

		ResponseEntity<ProductDTO> productsResponse = 
				new ResponseEntity<>(productDTO, HttpStatus.OK);
		
		given(productController.saveProduct(productDTO))
				.willReturn(productsResponse);

		mvc.perform(
				post(BASE_URI_PATH)
				.content(mapper.writeValueAsString(productDTO))
				.contentType(APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void putProduct_success() throws Exception {
		
		ResponseEntity<ProductDTO> productsResponse = 
				new ResponseEntity<>(productDTO, HttpStatus.OK);

		given(productController.updateProductByCode(productDTO.getProductCode(), productDTO))
		.willReturn(productsResponse);

		mvc.perform(
				put(BASE_URI_PATH + "" + productDTO.getProductCode())
				.content(mapper.writeValueAsString(productDTO))
				.contentType(APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void deleteProductByCode_success() throws Exception {

		given(productController.deleteProductByCode(productDTO.getProductCode()))
		.willReturn(HttpStatus.OK);

		mvc.perform(
				delete(BASE_URI_PATH + productDTO.getProductCode())
				.contentType(APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
}
