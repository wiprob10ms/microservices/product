package ms.bten.jarades.implementation.product.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ms.bten.jarades.implementation.product.dto.ProductDTO;
import ms.bten.jarades.implementation.product.entity.Product;
import ms.bten.jarades.implementation.product.repository.ProductRepository;
import ms.bten.jarades.implementation.product.service.impl.ProductServiceImpl;
import ms.bten.jarades.implementation.product.util.ServiceUtils;

@DataJpaTest
public class ProductServiceTest {
	
	@Mock
	ProductRepository productRepository;
	
	@InjectMocks
	ProductServiceImpl productService;
	
	Product product;
	ProductDTO productDTO;
	
	@BeforeEach
	public void init() {
		product = new Product(null, "SCNB6728", "Beer", "Heineken", "pure malt beer", "FOOD", 330, "ml");
		productDTO = ServiceUtils.convertFromObjectToClass(product, ProductDTO.class);
		productDTO.setInitQuantity(BigDecimal.TEN);
		productDTO.setBasePrice(BigDecimal.TEN);
	}
	
	@Test
	public void saveProductTest() {
		
		when(productRepository.save(product)).thenReturn(product);
		
		ProductDTO returnProduct = productService.saveProduct(productDTO);
		
		assertThat(returnProduct.getProductCode()).isEqualTo(product.getProductCode());
		
	};
	
	@Test
	public void getProductByCodeTest() {
		
		when(productRepository.findByProductCode(product.getProductCode())).thenReturn(Optional.of(product));
		
		Optional<ProductDTO> returnProduct = productService.getProductByCode(product.getProductCode());
		
		assertThat(returnProduct).isNotNull();
		assertThat(returnProduct.get().getProductCode()).isEqualTo(product.getProductCode());
		
	};

}